#-------------------------------------------------
#
# Project created by QtCreator 2012-06-23T16:12:29
#
#-------------------------------------------------

QT       += core gui

TARGET = Evolutiva
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    algoritmo.cpp

HEADERS  += mainwindow.h \
    algoritmo.h

FORMS    += mainwindow.ui
