/*
* Nombre archivo : algoritmo.cpp
* Nombre de la clase : Algoritmo
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Junio del 2012
* Fecha de modificación: Junio del 2012
*
* Cromosoma: un arreglo de tamaño n donde la posicion 0,1 es una pareja, la posicion 2 3 representa otra
* pareja y asi sucesivamente, el valor en esa posicion representa el lugar donde se encuentra un cubo determinado.
*
* Universidad del Valle
*/
#include "algoritmo.h"
#include <cstdlib>
#include <vector>
#include <iostream>
#include <cmath>

using std::vector;
using std::cout;
using std::endl;

Algoritmo::Algoritmo(int cantidadParejas, int cantidadGeneraciones, int cantidadIndividuos)
{
    this->cantidadParejas = cantidadParejas;
    this->cantidadGeneraciones = cantidadGeneraciones;
    this->cantidadIndividuos = cantidadIndividuos;

    this->poblacion = new int*[this->cantidadIndividuos];
    for(int i=0;i<this->cantidadIndividuos;i++)
    {
        this->poblacion[i] = new int[this->cantidadParejas*2];
    }

    k = ceil(this->cantidadIndividuos*0.1);//el tamaño del mating pool es aprox 10% la cantidad de cromosomas
    this->matingPool = new int*[k];

    this->encontroSolucion = false;

    srand(time(NULL));
}

Algoritmo::~Algoritmo()
{
    for(int i=0;i<this->cantidadIndividuos;i++)
    {
        delete this->poblacion[i];
        this->poblacion[i]=0;
    }

    delete this->poblacion;
    this->poblacion=0;

    for(int i=0;i<this->k;i++)
    {
        this->matingPool[i]=0;
    }

    this->matingPool=0;
}

void Algoritmo::setCantidadParejas(int cantidadParejas)
{
    this->cantidadParejas = cantidadParejas;
}

void Algoritmo::setCantidadGeneraciones(int cantidadGeneraciones)
{
    this->cantidadGeneraciones = cantidadGeneraciones;
}

void Algoritmo::setCantidadIndividuos(int cantidadIndividuos)
{
    this->cantidadIndividuos = cantidadIndividuos;
}

int Algoritmo::getCantidadParejas()
{
    return this->cantidadParejas;
}

int Algoritmo::getCantidadGeneraciones()
{
    return this->cantidadGeneraciones;
}

int Algoritmo::getCantidadIndividuos()
{
   return this->cantidadIndividuos;
}

void Algoritmo::generarPoblacionInicial()
{
    for(int i=0; i < this->cantidadIndividuos; i++)
    {
        int cantidadPosiciones = this->cantidadParejas*2;
        this->generarPosiciones();

        for(int j = cantidadPosiciones-1;j>=0;j--)
        {
            int r = rand() % cantidadPosiciones ;
            poblacion[i][j] = posiciones[r];
            posiciones.erase(posiciones.begin()+r);
            cantidadPosiciones--;
        }
    }
}

int Algoritmo::evaluacion(int* cromosoma)
{

    //Funcion de evaluacion 1

    /*
        int cont = 0;

        for(int i=0, pareja=1; i < this->cantidadParejas*2; i+=2, pareja++)
        {
            if(fabs(cromosoma[i]-cromosoma[i+1])-1 == pareja)
            {
                cont++;
            }
        }

        cout << "evaluacion finalizada "<< cont << endl;
    */


    // Funcion de evaluacion 2
    bool ocurrenciaDistancias[cantidadParejas+1];

    for(int i=0;i<cantidadParejas+1;i++)
    {
        ocurrenciaDistancias[i] = 0;
    }

    int cont = 0;

    for(int i=0; i < this->cantidadParejas*2; i+=2)
    {
        int valor = fabs(cromosoma[i]-cromosoma[i+1])-1;
        if( valor !=0 && ocurrenciaDistancias[valor] == 0 && valor<= cantidadParejas+1)
        {
            ocurrenciaDistancias[valor]= 1;
            cont++;
        }
    }

    return cont;
}

void Algoritmo::mutar(int* cromosoma)
{
    //la técnica de mutacion ese seleccionar aleatoriamente dos genes del cromosoma y hacer swap
    int index1 = rand()%this->cantidadParejas*2;
    int index2 = rand()%this->cantidadParejas*2;
    int temp = cromosoma[index1];
    cromosoma[index1] = cromosoma[index2];
    cromosoma[index2] = temp;
}

void Algoritmo::imprimirPoblacion()
{
    for(int i=0; i < this->cantidadIndividuos; i++)
    {
        for(int j=0;j < cantidadParejas*2;j++)
        {
            cout << poblacion[i][j] << " ";
        }
        cout << endl;
    }
}

void Algoritmo::buscar()
{
    this->generarPoblacionInicial();

    for(int i=0;i<this->cantidadGeneraciones && !this->encontroSolucion;i++)
    {
        this->seleccionar();
        this->reproducir();
    }
}

void Algoritmo::generarPosiciones()
{
    int cantidadPosiciones = this->cantidadParejas*2;
    for(int i=1;i<=cantidadPosiciones;i++)
    {
        this->posiciones.push_back(i);
    }
}

void Algoritmo::seleccionar()
{
    //los cromosomas se seleccionaran por torneo

    int tamTemporal = ceil(this->cantidadIndividuos*0.03);//se tomará el 3% de los individuos

    for(int i=0,index;i<k && !this->encontroSolucion;i++)
    {
        int puntajeMejor = 0;

        for(int j=0;j<tamTemporal;j++)
        {
            index = rand()%this->cantidadIndividuos;
            int eval = this->evaluacion(this->poblacion[index]);
            this->indexSolucion = index;
            if(eval > puntajeMejor)
            {
                if(eval == this->cantidadParejas)//ha encontrado una solucion, no se itera más
                {
                    this->encontroSolucion = true;
                    break;
                }else
                {
                    puntajeMejor = eval;
                }
            }
        }

        this->matingPool[i] = this->poblacion[this->indexSolucion];
    }
}

void Algoritmo::reproducir()
{
    //al momento de mutar, como el cromosoma es un puntero a un cromosoma de la población, este se verá afectado
    //por lo que la técnica de reemplazo es los hijos por los padres y no existe un reemplazo explicito

    for(int i=0;i<k && !this->encontroSolucion;i++)
    {
        this->mutar(this->matingPool[i]);
    }
}

QString Algoritmo::getSolucion()
{

    if(!encontroSolucion)
    {

        int puntajeMejor = 0;

        for(int i=0;i<this->cantidadIndividuos;i++)
        {
            int eval = this->evaluacion(this->poblacion[i]);
            if(eval > puntajeMejor)
            {
                puntajeMejor = eval;
                this->indexSolucion = i;
            }
        }
    }

    vector<int> temp(this->cantidadParejas*2);
    QString solucion = "";

    for(int i=0,pareja=1;i<this->cantidadParejas*2;i+=2,pareja++)
    {
        int diferencia = fabs(poblacion[this->indexSolucion][i] - poblacion[this->indexSolucion][i+1])-1;
        temp[this->poblacion[this->indexSolucion][i]-1] = pareja;
        temp[this->poblacion[this->indexSolucion][i+1]-1] = pareja;
        cout << "Pareja "<< pareja <<" Diferencia : " <<diferencia <<endl;

    }


    for(int i=0;i<this->cantidadParejas*2;i++)
    {
        solucion += QString::number(temp[i]) + " ";
    }

    cout << "Cantidad de aciertos para las diferencias de los cubos encontrada: " << evaluacion(poblacion[indexSolucion])<< endl;
    //imprimirPoblacion();

    return  solucion;
}

bool Algoritmo::getEncontroSolucion()
{
    return this->encontroSolucion;
}
