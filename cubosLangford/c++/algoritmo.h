/*

* Nombre archivo : algoritmo.h
* Nombre de la clase : Algoritmo
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Junio del 2012
* Fecha de modificación: Junio del 2012
*
*
* Universidad del Valle
*/


#ifndef ALGORITMO_H
#define ALGORITMO_H

#include <vector>
#include <QString>

class Algoritmo
{
private:
    int cantidadParejas;
    int cantidadGeneraciones;
    int cantidadIndividuos;
    int k;//tamaño del mating pool
    int **poblacion;
    int **matingPool;
    int indexSolucion;
    bool encontroSolucion;
    std::vector<int> posiciones;

    void generarPoblacionInicial();
    void generarPosiciones();
    int evaluacion(int *cromosoma);
    void mutar(int *cromosona);
    void seleccionar();
    void reproducir();

public:
    Algoritmo();
    Algoritmo(int cantidadParejas, int cantidadGeneraciones = 100, int cantidadIndividuos = 100);
    ~Algoritmo();

    void setCantidadParejas(int cantidadParejas);
    void setCantidadGeneraciones(int cantidadGeneraciones);
    void setCantidadIndividuos(int cantidadIndividuos);
    int getCantidadParejas();
    int getCantidadGeneraciones();
    int getCantidadIndividuos();
    bool getEncontroSolucion();
    void imprimirPoblacion();
    void buscar();
    QString getSolucion();
};

#endif // ALGORITMO_H
