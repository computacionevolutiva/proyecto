/*

* Nombre archivo : mainwindow.h
* Nombre de la clase : Mainwindow
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Junio del 2012
* Fecha de modificación: Junio del 2012
*
*
* Universidad del Valle
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void run();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
