/*
* Nombre archivo : mainwindow.h
* Nombre de la clase: Mainwindow
* Autor: Maria Andrea Cruz Blandon
         Cristian Leonardo Rios
         Yerminson Doney Gonzalez Muñoz
* Fecha de creación: Junio del 2012
* Fecha de modificación: Junio del 2012
*
*
* Universidad del Valle
*/


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "algoritmo.h"
#include <QTextCodec>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->ejecutar,SIGNAL(clicked()),this,SLOT(run()));

    QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(linuxCodec);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::run()
{
    int cantidadParejas = ui->cantidadParejas->text().toInt();
    int cantidadIndividuos = ui->cantidadIndividuos->text().toInt();
    int cantidadGeneraciones = ui->cantidadGeneraciones->text().toInt();

    Algoritmo a(cantidadParejas,cantidadGeneraciones,cantidadIndividuos);
    a.buscar();
    ui->solucion->setText(a.getSolucion());

    if(a.getEncontroSolucion())
    {
        ui->mensaje->setText(tr("Se a encontrado solución exacta!!!"));
    }else
    {
        ui->mensaje->setText(tr("Se a encontrado una solucion aproximada!!!"));
    }
}
