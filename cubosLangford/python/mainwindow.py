#Cristian Leonardo Ríos López
#Técnicas heurísticas y metaheurísticas
#Máster en Matemáticas y Computación
#Universidad de Cantabria

import tkinter as tk
from algoritmo import Algoritmo

class MainWindow(tk.Frame):

    def __init__(self, master=None):
        super().__init__(master)
        self.cantidadParejasEntry = tk.StringVar()
        self.cantidadIndividuosEntry = tk.StringVar()
        self.cantidadGeneracionesEntry = tk.StringVar()
        self.mensaje = tk.StringVar()
        self.solucion = tk.StringVar()
        self.informacion = tk.StringVar()
        self.config(bg="black") # Le da color al fondo
        self.pack()
        self.crearWidgets()
        self.valoresDefecto()

    def crearWidgets(self):
        fontetiquetas = ('Helvetica',14,'bold')
        fontentry = ('Helvetica',11,'bold')
        coloretiquetas = '#0080FF'
        colormensajes='#D0003E'

        self.label_titulo = tk.Label(
            self, 
            text="Cubos de Lagford\nAlgoritmo Evolutivo",
            borderwidth=1,
            bg='black',
            font=('Helvetica',24,'bold'),
            fg='purple',
            width=25
            )
        self.label_individuos = tk.Label(
            self,
            text="Cantidad de individuos:",
            bg='black',
            fg=coloretiquetas,
            font=fontetiquetas,
            )
        self.entry_individuos = tk.Entry(
            self,
            textvariable=self.cantidadIndividuosEntry,
            fg=coloretiquetas,
            font=fontentry
            )
        self.label_generaciones = tk.Label(
            self,
            text="Cantidad de generaciones:",
            bg='black',
            fg=coloretiquetas,
            font=fontetiquetas,
            )
        self.entry_generaciones = tk.Entry(
            self,
            textvariable=self.cantidadGeneracionesEntry,
            fg=coloretiquetas,
            font=fontentry
            )
        self.label_parejas = tk.Label(
            self,
            text="Cantidad de parejas de cubos:",
            bg='black',
            fg=coloretiquetas,
            font=fontetiquetas,
            )
        self.entry_parejas = tk.Entry(
            self,
            textvariable=self.cantidadParejasEntry,
            fg=coloretiquetas,
            font=fontentry
            )
        self.button_encontrar = tk.Button(
            self,
            text="Encontrar solución",
            command=self.encontrarSolucion,
            font=fontentry,
            fg='purple',
            activeforeground='purple'
            )
        self.button_limpiar = tk.Button(
            self,
            text="Limpiar"
            )
        self.button_restaurar = tk.Button(
            self,
            text="Restaurar"
            )
        self.label_mensaje = tk.Label(
            self,
            textvariable=self.mensaje,
            fg=colormensajes,
            font=fontentry,
            bg='black'
            )
        self.label_solucion = tk.Label(
            self,
            textvariable=self.solucion,
            bg='black',
            font=fontetiquetas,
            fg=coloretiquetas,
            relief=tk.GROOVE
            )
        self.label_informacion = tk.Label(
            self,
            textvariable=self.informacion,
            bg='black',
            font=fontentry,
            fg=colormensajes
            )

        self.label_titulo.grid(row=0,column=0,columnspan=2,pady=(10,20))
        self.label_individuos.grid(row=1,column=0,sticky=tk.W,padx=10)
        self.entry_individuos.grid(row=1,column=1,padx=10)
        self.label_generaciones.grid(row=2,column=0,sticky=tk.W,padx=10)
        self.entry_generaciones.grid(row=2,column=1)
        self.label_parejas.grid(row=3,column=0,sticky=tk.W,padx=10)
        self.entry_parejas.grid(row=3,column=1)
        self.button_encontrar.grid(row=4,column=0,columnspan=2,pady=10)
        self.label_mensaje.grid(row=5,column=0,columnspan=2,pady=10)
        self.label_solucion.grid(row=6,column=0,columnspan=2,pady=10)
        self.label_informacion.grid(row=7,column=0,columnspan=2,pady=10)

    def encontrarSolucion(self):
        try:
            cantidadParejas = int(self.cantidadParejasEntry.get())
            cantidadIndividuos = int(self.cantidadIndividuosEntry.get())
            cantidadGeneraciones = int(self.cantidadGeneracionesEntry.get())

            a = Algoritmo(cantidadParejas,cantidadIndividuos,cantidadGeneraciones)
            a.buscar()
            if a.getEncontroSolucion():
                self.mensaje.set("Se ha encontrado una solución exacta!!!")
            else:
                self.mensaje.set("Se ha encontrado una solución aproximada!!!\nLos 0 indica cubos que no están en el lugar correcto.")

            sol = a.getSolucion()
            solsrt = "| "
            for i,ele in enumerate(sol):
                solsrt += str(ele) + " | "
                if (i+1) % 20 == 0:
                    solsrt += "\n"
            self.solucion.set(solsrt)

            generaciones = a.getGeneracionesReal()
            valor = a.getValorSolucion()
            info = "Cantidad de Generaciones: " + str(generaciones) + "\n"
            info += "Cantidad de parejas correctas: " + str(valor)
            self.informacion.set(info)

        except ValueError as e:
            self.mensaje.set("Los valores dados no son correctos!")
            self.solucion.set("")
            self.informacion.set("")

    def valoresDefecto(self):
        self.cantidadParejasEntry.set(4)
        self.cantidadGeneracionesEntry.set(100)
        self.cantidadIndividuosEntry.set(100)



root = tk.Tk()
#root.geometry("500x500") # Cambia el tamaño de la ventana
app = MainWindow(master=root)
app.master.title("Cubos de Lagford")
#app.master.maxsize(1000, 400)
app.mainloop()