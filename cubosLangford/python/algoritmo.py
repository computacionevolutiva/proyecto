#Cristian Leonardo Ríos López
#Técnicas heurísticas y metaheurísticas
#Máster en Matemáticas y Computación
#Universidad de Cantabria

import numpy as np
import math
import random

class Algoritmo:
    def __init__(self,cantidadParejas,cantidadIndividuos,cantidadGeneraciones):
        self.cantidadParejas = cantidadParejas
        self.cantidadGeneraciones = cantidadGeneraciones
        self.cantidadIndividuos = cantidadIndividuos
        self.poblacion = np.zeros((self.cantidadIndividuos,self.cantidadParejas*2),dtype=int)
        self.k = math.ceil(self.cantidadIndividuos*0.1);#el tamaño del mating pool es aprox 10% la cantidad de cromosomas
        self.matingPool = np.zeros(self.k,dtype=int)
        self.encontroSolucion = False

    def generarPoblacionInicial(self):
        for i in range(self.cantidadIndividuos):
            posiciones = list(range(1,self.cantidadParejas*2+1))
            for j in range(self.cantidadParejas*2):
                r = random.randint(0,len(posiciones)-1)
                self.poblacion[i,j] = posiciones.pop(r)

    def evaluacion(self, cromosoma):
        ocurrenciaDistancias = np.zeros(self.cantidadParejas)
        cont = 0
        i = 0
        while i < self.cantidadParejas*2:
            valor = int(math.fabs(cromosoma[i]-cromosoma[i+1])-1)
            if valor != 0 and valor <= self.cantidadParejas and ocurrenciaDistancias[valor-1] == 0:
                ocurrenciaDistancias[valor-1] = 1
                cont += 1
            else:
                break
            i+=2
        return cont

    def mutar(self,cromosoma):
        #la técnica de mutacion ese seleccionar aleatoriamente dos genes del cromosoma y hacer swap
        top = self.cantidadParejas*2-1
        index1 = random.randint(0,top)
        index2 = random.randint(0,top)
        temp = cromosoma[index1]
        cromosoma[index1] = cromosoma[index2]
        cromosoma[index2] = temp

    def seleccionar(self):
        #los cromosomas se seleccionan por torneo
        tamTemporal = math.ceil(self.cantidadIndividuos*0.03) #se tomará el 3% de los individuos
        i = 0
        j = 0
        while i < self.k and not self.encontroSolucion:
            puntajeMejor = -1
            while j < tamTemporal:
                index = random.randint(0,self.cantidadIndividuos-1)
                evalc = self.evaluacion(self.poblacion[index])
                if evalc > puntajeMejor:
                    self.indexSolucion = index
                    if evalc == self.cantidadParejas: #se ha encontrado una solución optima y no se itera más
                        self.encontroSolucion = True
                        break
                    else:
                        puntajeMejor = evalc
                j+=1
            self.matingPool[i] = self.indexSolucion#no representa la solucion global porque puede que al sigueinte torneo haya alguien peor que el general pero mejor en el torneo
            i+=1

    def reproducir(self):
        #al momento de mutar, como el cromosoma es pasado por referencia, este se verá afectado directamente en la población
        #por lo que la técnica de reemplazo es los hijos por los padres y no existe un reemplazo explicito
        i = 0
        while i < self.k and not self.encontroSolucion:
            self.mutar(self.poblacion[self.matingPool[i]])
            i+=1

    def buscar(self):
        self.generarPoblacionInicial()
        i = 0
        while i < self.cantidadGeneraciones and not self.encontroSolucion:
            self.seleccionar()
            self.reproducir()
            i+=1
            self.cantidadGeneracionesReal = i
        if not self.encontroSolucion:
            i = 0
            puntajeMejor = -1
            while i < self.cantidadIndividuos:
                evalc = self.evaluacion(self.poblacion[i])
                if evalc > puntajeMejor:
                    puntajeMejor = evalc
                    self.indexSolucion = i
                i+=1

    def getSolucion(self):
        solucion = self.poblacion[self.indexSolucion]
        solucionExpresada = np.zeros(self.cantidadParejas*2, dtype=int)
        i = 0
        distancias = list(range(1,self.cantidadParejas+1))
        while i < self.cantidadParejas*2-1:
            a = solucion[i]
            b = solucion[i+1]
            n = int(math.fabs(a-b)-1)
            if n in distancias:
                solucionExpresada[a-1] = n
                solucionExpresada[b-1] = n
                distancias.remove(n)
            else:
                solucionExpresada[a-1] = 0
                solucionExpresada[b-1] = 0
            i+=2

        #print("solucion",self.poblacion[self.indexSolucion])
        #print("valor",self.evaluacion(self.poblacion[self.indexSolucion]))
        #print("generaciones",self.cantidadGeneracionesReal)
        #print("solucion expresada",solucionExpresada)

        return solucionExpresada

    def getEncontroSolucion(self):
        return self.encontroSolucion

    def getGeneracionesReal(self):
        return self.cantidadGeneracionesReal

    def getValorSolucion(self):
        return self.evaluacion(self.poblacion[self.indexSolucion])
