#Maximización de funciones
#Autor: Cristian Leonardo Ríos López
#Asignatura: Computación evolutiva
#Fecha: Septiembre 27 de 2015

import random
import math

class Funcion:
	def __init__(self,tipo):
		self.tipo = tipo

	def evaluar(self,x):
		if self.tipo == 1:
			return 20 + math.e - 20*math.exp(-0.2*math.fabs(x)) - math.exp(math.cos(2*math.pi*x))

	def imprimirMaximo(self):
		if self.tipo == 1:
			print("El valor máximo de la función se alcanza en 31.5005 y es: ", self.evaluar(31.5005))


class Individuo:
	def __init__(self, cantidadGenes, xMin, xMax, funcion):
		self.xMin = xMin
		self.xMax = xMax
		self.cantidadGenes = cantidadGenes
		self.genes = self.crearGenes()
		self.x = self.decodificarX()
		self.adaptacion = self.calcularAdaptacion(funcion)

	def crearGenes(self):
		genes = []
		for i in range(self.cantidadGenes):
			x = random.random()
			if x < 0.5:
				genes.append(0)
			else:
				genes.append(1)
		return genes

	def bin2Ent(self):
		ent = 0
		posicion = 1
		for i in range(self.cantidadGenes):
			ent += posicion*self.genes[self.cantidadGenes-1-i]
			posicion *= 2
		return ent

	def decodificarX(self):
		x = self.bin2Ent()
		return self.xMin + x*((self.xMax-self.xMin)/((2**self.cantidadGenes) - 1))

	def calcularAdaptacion(self, funcion):
		return funcion.evaluar(self.x)

	def actualizar(self,funcion):
		self.x = self.decodificarX()
		self.adaptacion = self.calcularAdaptacion(funcion)

	def imprimir(self):
		print("Cromosoma: ", self.genes)
		print("Fenotipo: ", self.x)
		print("Adaptación: ", self.adaptacion)

	def getAdaptacion(self):
		return self.adaptacion

	def getX(self):
		return self.x

	def getCantidadGenes(self):
		return self.cantidadGenes

class Optimizacion:
	def __init__(self,tamanoPoblacion,numeroGeneraciones,probCruce,probMutacion,precision,elitismo,funcion,xMin,xMax):
		''' precision: con cuanta precision desea optener el resultado
		elitismo: si se va a efectuar o no elitismo(Aun no es usada)
		funcion: objeto del tipo funcion que deseamos optimizar'''

		self.tamanoPoblacion = tamanoPoblacion
		self.numeroGeneraciones = numeroGeneraciones
		self.probCruce = probCruce
		self.probMutacion = probMutacion
		self.precision = precision
		self.elitismo = elitismo
		self.funcion = funcion
		self.k = math.ceil(tamanoPoblacion*.1)#tamaño del mating pool, 10% de la poblacion
		self.matingPool = []

		longitudCromosoma = math.ceil(math.log(1+((xMax-xMin)/precision),2))
		self.poblacion = self.poblacionInicial(longitudCromosoma,xMin,xMax)

		#el tamaño del mating pool se hace par para asegurar que los individuos se puedan reproducir
		if self.k%2 != 0:
			self.k+=1

	def poblacionInicial(self,longitudCromosoma,xMin,xMax):
		poblacion = []
		for i in range(self.tamanoPoblacion):
			x = Individuo(longitudCromosoma,xMin,xMax,self.funcion)
			poblacion.append(x)
		return poblacion

	def evaluar(self):
		for i in range(self.numeroGeneraciones):
			self.seleccion()
			self.reproduccion()
		self.encontrarMejor()

	def seleccion(self):
		#la selección se realiza por torneo entre dos cada vez
		for i in range(self.k):
			x1 = random.randint(0,self.tamanoPoblacion-1)
			ind1 = self.poblacion[x1]
			x2 = random.randint(0,self.tamanoPoblacion-1)
			ind2 = self.poblacion[x2]
			#guardo las posiciones, ocupan menos espacio que los individuos
			if ind1.getAdaptacion() > ind2.getAdaptacion():
				self.matingPool.append(x1)
			else:
				self.matingPool.append(x2)

	def reproduccion(self):
		for i in range(self.k//2):
			a = random.random()
			if a<self.probCruce:
				self.cruce(i,i+1)
		for i in range(self.k):
			self.mutacion(i)
		self.matingPool = []

	def mutacion(self,ind):
		mutado = False
		individuo = self.poblacion[self.matingPool[ind]]
		for i in range(individuo.getCantidadGenes()):
			a = random.random()
			if a < self.probMutacion:
				individuo.genes[i] = int(not individuo.genes[i])
				mutado = True
		if mutado:
			individuo.actualizar(self.funcion)
			self.poblacion[self.matingPool[ind]] = individuo

	def cruce(self, ind1,ind2):
		padre1 = self.poblacion[self.matingPool[ind1]]
		padre2 = self.poblacion[self.matingPool[ind2]]
		corte = random.randint(0,padre1.getCantidadGenes()-1)
		temp = []
		for i in range(corte):
			temp = padre1.genes[i]
			padre1.genes[i] = padre2.genes[i]
			padre2.genes[i] = temp
		padre1.actualizar(self.funcion)
		padre2.actualizar(self.funcion)
		#reemplazo a un individuo de la poblacion aleatorioamente, también podría haber reemplazado a los padres
		self.poblacion[random.randint(0,self.tamanoPoblacion-1)] = padre1
		self.poblacion[random.randint(0,self.tamanoPoblacion-1)] = padre2
		#self.poblacion[self.matingPool[ind1]] = padre1
		#self.poblacion[self.matingPool[ind2]] = padre2

	def encontrarMejor(self):
		adaptacion = 0
		self.mejor = None
		for i in range(self.tamanoPoblacion):
			x = self.poblacion[i].getAdaptacion()
			if x > adaptacion:
				adaptacion = x
				self.mejor = self.poblacion[i]

	def imprimirMejor(self):
		#para este caso coincide que el valor de la adaptacion es el mismo que el valor que estamos buscando(máximo de una función)
		if self.mejor:
			print("El valor de x es: ", self.mejor.getX(), " y su evaluacion es: ", self.mejor.getAdaptacion())
		else:
			print("No se ejecutado el algoritmo, aún no existe un mejor")

	def imprimirPoblacion(self):
		for i in range(self.tamanoPoblacion):
			print("Individuo: ",i)
			self.poblacion[i].imprimir()
			print()




#Ejecución

f = Funcion(1)
f.imprimirMaximo()

tamanoPoblacion = 100
numeroGeneraciones = 100
probCruce = 0.4
probMutacion = 0.2
precision = 0.0001
elitismo = False
#intervalo de maximización
xMin = 0
xMax = 32

opt = Optimizacion(tamanoPoblacion,numeroGeneraciones,probCruce,probMutacion,precision,elitismo,f,xMin,xMax)
opt.evaluar()
#opt.imprimirPoblacion()
opt.imprimirMejor()


